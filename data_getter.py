# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 15:28:07 2024

@author: thefr
"""
from bob import *
import csv
import os
class getData:
    titre_user = None
    titre_book = None
    titre_has_book = None
    def __init__(self , titre_user , titre_book , titre_has_book):
        self.titre_user = os.path.dirname(__file__)+titre_user
        self.titre_book = os.path.dirname(__file__)+titre_book  
        self.titre_has_book = os.getcwd()+titre_has_book
        
    def get_user(self):
        list_user=[]
        with open(self.titre_user, mode ='r')as file:
          csvFile = csv.DictReader(file)
          for lines in csvFile:
              print(lines)
              list_user.append( User(lines['id'],lines['email'],lines['password']) )
        return list_user
        
    def get_book(self):
        list_book = []
        with open(self.titre_book, mode ='r')as file:
          csvFile = csv.DictReader(file)
          for lines in csvFile:
              print(lines)
              list_book.append( Book(lines['id'],lines['author'],lines['title'], lines['content'],lines['available_in_store'],lines['store_price']) )
        return list_book;
        
    def get_ser_has_book(self):
        list_has_book = []
        with open(self.titre_has_book, mode ='r')as file:
          csvFile = csv.reader(file)
          for lines in csvFile:
              list_has_book.append( User_has_book(lines[0],lines[1],lines[2],lines[3],lines[4]) );
        return list_has_book;
    
