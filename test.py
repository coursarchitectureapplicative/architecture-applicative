from bob import *
from data_getter import*
import unittest
class TestUniter(unittest.TestCase):
    def test_user(self):
        mail = 'frederic.larue17@gmail.com'
        pissword = 'password'
        user = User(0,mail,pissword)
        self.assertEqual( user.email , mail )
        self.assertEqual( user.password , pissword )
        
    def test_livre(self):
        titre = "100 metre sous mer"
        content = "https://exist/pas"
        autheur= "Jules Verne"
        dans_le_magasin  = True
        prix= 1000
        livre =Book( titre , content , autheur , dans_le_magasin)
        self.assertEqual( livre.titre , titre )
        self.assertEqual( livre.content , content )
        self.assertEqual( livre.autheur , autheur )
        self.assertEqual( livre.dans_le_magasin , dans_le_magasin )
        self.assertEqual( livre.prix , prix )
        
    def test_livre(self):
        id_user = 0
        id_livre = 0
        purchase_date = "10/01/2021"
        medium = 1
        id=0
        user_has_book = User_has_book (id, id_user, id_livre, purchase_date, medium)
        self.assertEqual( user_has_book.id_user , id_user )
        self.assertEqual( user_has_book.id_livre , id_livre )
        self.assertEqual( user_has_book.purchase , purchase_date )
        self.assertEqual( user_has_book.medium , medium )
        
    def test_check_id(self):
        get_Data = getData( r"/resources/users.csv"  , r"/resources/books.csv" , r"/resources/users_have_books.csv" )
        list_user = get_Data.get_user()
        list_book = get_Data.get_book()
        list_has_book = get_Data.get_ser_has_book()
        
        id_user=[]
        id_livre=[]
        
        for ele in list_user:
            id_user.append(ele.id)

        for ele in list_book:
            id_livre.append(ele.id)
            
        for ele in list_has_book:
            self.assertTrue(ele.id_user in id_user , f'{ele.id_user}'  )
            self.assertTrue(ele.id_livre in id_livre)
            
            