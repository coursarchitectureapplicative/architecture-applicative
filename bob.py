class User :
    def __init__(self, id, email, password):
        self.__id = id
        self.__email = email
        self.__password = password

    @property
    def id(self):
        return self.__id

    @property
    def email(self):
        return self.__email

    @email.setter
    def email(self, value):
        self.__email = value

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, value):
        self.__password = value

    @password.getter
    def password(self):
        return self.__password
class Book :
    def __init__(self, id, title, content, author, in_shop, price):
        self.__id = id
        self.__title = title
        self.__content = content
        self.__author = author
        self.__in_shop = in_shop
        self.__price = price

    @property
    def id(self):
        return self.__id

    @property
    def title(self):
        return self.__title

    @property
    def content(self):
        return self.__content

    @property
    def author(self):
        return self.__author

    @property
    def in_shop(self):
        return self.__in_shop

    @property
    def price(self):
        return self.__price

    @title.setter
    def title(self, value):
        self.__title = value

    @content.setter
    def content(self, value):
        self.__content = value

    @author.setter
    def author(self, value):
        self.__author = value

    @in_shop.setter
    def in_shop(self, value):
        self.__in_shop = value

    @price.setter
    def price(self, value):
        self.__price = value

class User_has_book :
    def __init__(self, id, id_user, id_livre, purchase, medium):
        self.__id = id
        self.__id_user = id_user
        self.__id_livre = id_livre
        self.__purchase = purchase
        self.__medium = medium


    @property
    def id(self):
        return self.__id

    @property
    def id_user(self):
        return self.__id_user

    @property
    def id_livre(self):
        return self.__id_livre

    @property
    def purchase(self):
        return self.__purchase

    @property
    def medium(self):
        return self.__medium


    @purchase.getter
    def purchase(self):
        return self.__purchase 

    @id_user.getter
    def id_user(self):
        return self.__id_user

    @id_livre.getter
    def id_livre(self):
        return self.__id_livre 

    @medium.getter
    def medium(self):
        return self.__medium 

